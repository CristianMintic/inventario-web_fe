import { createRouter, createWebHashHistory } from 'vue-router'
import IngresoInventario from "../components/IngresoInventario.vue";
import SalidaInventario from "../components/SalidaInventario.vue";
import VisualizarInventario from "../components/VisualizarInventario.vue";

const routes = [
  {
    path: "/ingreso",
    name: "ingresoI",
    component: IngresoInventario,
  },
    {
    path: "/salida",
    name: "salidaI",
    component: SalidaInventario,
    props: true,
  },
  {
    path: "/visualizar",
    name: "visualizarI",
    component: VisualizarInventario,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router
